---
title: New year, new site!
tags: meta
---

Hey all! It's a new year (well almost, anyway), and I've finally buckled down and started building a homepage for myself. This site is **still under construction**; it's slowly being built up from the infrastructure Ash built over at [heyquark.com](https://heyquark.com). (Go check him out - he's a cool guy!) Pardon the mess as I shift things around, and...(I feel *so* proud to finally say this)...welcome to my homepage!

***

...Ah right, I should put something under the break so that "read more" button isn't quite so pointless. Let's talk about the new site, I guess! The closest thing I've had to a homepage in the past was my Tumblr blog, and that was quite the mess indeed. I've been working with my good friend Ash (a.k.a. QuarkTheAwesome) to build up a new homepage of my own, with some design considerations in mind:

- Lightweight. (This borrows from [heyquark.com](https://heyquark.com)'s philosophy: this should load nice and quickly on pretty much any device, new or old.)
- Extensible. (I've got the typical blog format here, but I'll be able to modify this page to serve whatever needs I may encounter. I can't do that with a blogging service.)
- Low-maintenance. (This entire thing is hosted using GitLab Pages and cached using Cloudflare. I don't mind server admin work, but this will stay up even if my home server goes down. It's also editable from anywhere....and hosted for free!)
- Ethical. (I don't track your data without telling you, and neither will the ads! Ads are served by [heyquark ads](https://ads.heyquark.com): curated, ethical, and not-for-profit. I seriously don't make a dime off of this!)

There's probably something my 2AM brain is forgetting right now...I might have to update this later. (I probably will, in fact.)