---
title: "Perspective, or the beauty of an older Internet"
category: blag
tags: journaling retrospective exposition
comments: true
---

Moments spent reading through the online journals of Internet users before me have led me to an admittedly obvious realization: this truly will be a snapshot of my mindset like no retrospective could ever be.

***

I took a short bit of time (well, okay, more than a little) to read through the online archives of a few people I know: specifically, webpages and blog entries of Internet users from a previous decade. That, in and of itself is a fascinating exploration, snapshotting the culture and life of a previous time, a previous Internet - however, what specifically caught me was the content that these users had written. Some were on hosted blogging services, and some had set up their own webpages. Some were showcase pages meant as an individual's public broadcast; others were merely public subsets of a private journal. 

Above the individual idiosyncracies, however, one thing stood clear: each subject line, each journal entry, each carefully crafted comment served to build a story - a description of who these people were, and how they grew over time. Especially on more social shared hosting platforms, relationships and interactions between friends would become clear, painting an image beyond each user's isolated world. With enough time, I found myself forming a deeper understanding of the slice of life each very real person lived in. (Even the frequency of someone's posts gave a measure of insight, assisting to tell the story of someone's interests, or how their time was spent.)

With time, I hope this page might fulfill a similar goal. Granted, this is a public site, so it will never become as intimate of an archive as, per se, the private entries of an older LiveJournal. Still, it serves as an unknowing time capsule, a monument to each passing iteration of my mind.

***

In saying this, I feel that a little blurb of exposition is obligatory - mandatory, even. (To narrate for friends in the present and remind myself in the future - hi, by the way!) Here goes...

- My name is Alex, widely known in online circles as CompuCat. Since...2010, if old Google Sites logs are correct. (I have alts, though I rarely use them.)
- I'm sitting in control of a ridiculous computing setup, armed to handle incredible tasks for both computer-science and musical workloads...writing this entry in a humble instance of `nano`. (Granted, there's a `tmux` involved too...)
- The desk around me is cluttered - what little space I have on it, that is, given that my Yamaha keyboard takes up most of the available space. My plywood 12U rack (which serves as my side table) is in equal disarray, home to a bag of chips, three game consoles, a random book from the recycling, and an IDE DVD drive. 
- (The chips are Kettle brand. Salt-and-pepper, a flavor that's one of my insurmountable vices...though I wouldn't have it any other way.)
- Life is at a point of stress. I sit two days away from two project deadlines, six away from a debut gig as a bassist (playing with a local singer-songwriter), and I've wasted far too much time fribbling my stress into Tetris. It seems that my desk and mind share equal mental states.
- Constants that surround me: my guitar (that I play terribly), my Wii U (a point of connection to friends past and newfound), a keyboard (lately, my S90 ES), a hairbrush (badly in need of replacement), a little dragon figurine. (I'll add an artsy photo if I remember.)
- Current problem, solvable by money: replacement stylus for my AT-LP60.
- Current problem, solvable by introspection: a desire for better mental focus and grit.
- Current problem, solvable by...good question: a desire for a relationship. (Eventually...the last time something relationshippy happened led to a discovery of how I can be manipulated. It's not something I desperately need...indeed, this is a case where "desperate need" is a dangerous road. Still, it would be fulfilling.)
- Dream, fulfilled by a fortunate sequence of events[^1]: my entry to the broader world of professional sound reinforcement. (Specific thanks to Matthias, guru and hilarious partner in conversation...and to Lauren, whose tacos and limeade are to die for.)
- Aspiration, fulfilled by some amazing kindness, trust, and geographic convenience: making and meeting (in person!) some friends in the TAS community. (And, hopefully, again in the near future.)

And, to cap it off with a good ol' cliche...current mood: reflective. Inspired, but scatterbrained. Tired.

***

Erm, what was I writing about? As much as I find exposition draining, for once I'm having to pull myself away from killing the broader flow of this entry with Too Much Exposition™. :)[^2] 

Let's see: this fantastic essay so far has a block of introduction - an elaborate thesis, if you will...and a mere tangential block of personal exposition.

<center> <i>Lovely.</i> *sigh* </center> <!-- I love the fact that I can just drop HTML in like this. Annoying, but essentially handy. -->

With my mind centered, why is it that personal websites, blogs, online journals, and the like are so poigniantly perfect to effortlessly preserve the human story of a mere Internet user? In my opinion: a young, receptive audience, a burgeoning platform, a convenient place for self-expression, and just enough visibility. 

Consider the personal diary or journal: a humble handwritten chronicle of the owner's daily life. These tiny tomes provided a place of solace: their contents often available only to the owner, making them a perfect place for private expression. Thoughts so private that they might never leave the mind were given a place of restful permanence. 

Personal websites and blogs provided an interesting counterpoint: like diaries, they serve as an episodic, text-based archive of one's personal thoughts. However, early-2000s blog tools and platforms facilitated easy authoring - especially for those like me, whose typing is much faster (and more legible) than longhand chicken-scratching. With anything, ease of use drives frequency of use, and frequency similarly drives maturity in the way one uses a platform. We grow beyond the "Hello World!" and first "Dear Diary" entries and move to more fluent content, treating the medium as a place of familiarity, rather than a novel unknown. 

Though these public pages would never quite serve the same purpose as a completely private diary, their introduction was quickly welcomed as an attractive platform of self-expression. This was absolutely a function of the population of the Internet at the time: users mature enough for introspection and self-discovery, but young enough to quickly embrace exciting new platforms. Users with at least a bare minimum of technical knowledge. Users of a platform no longer exclusive to only technological pioneers, but not yet flooded by the masses. In short: a fascinating community of individuals, each with stories to share.

The key, though: given the Internet's developing state and population, posts written on these platforms were subject to an amount of public discovery. Enough to generate social traffic, but not so much that the platforms became mere cesspools of immediate public broadcast. Accessible enough for friends (and occasional passers-by) to read, but not directly plastered onto the front page of the news. The platform and culture as a whole formed a sort of Goldilocks zone: a balance of social discovery and trusted privacy.

It's this zone of private discovery that led to the beautiful development beyond the private, handwritten diaries preceding these user pages. While not as comprehensive as in a diary, truly read by none other than the author, the relative privacy of the blog still allowed for one to comfortably write about their headspace and frame of mind. More importantly, though, the slight amount of visibility allowed a network of comments and not-private-yet-not-wholly-public messages between users to blossom - the same network I touched on in the introduction of this entry. (Arguably, the slight amount of visibility might have even encouraged certain users to write more, knowing that their words were not merely shouts into a paperback void, but indeed created with the purpose of being read.) Within this perfect combination of the personal and interpersonal, frames began to form of the pictures of people behind each user. 

***

I don't think this sort of prolonged time-capsule is commonly possible with the current state of Internet culture. A paradoxic statement, one might say, considering the hyper-social, over-sharing culture of the 2010s: now that we're sharing more information than ever, why *wouldn't* we be able to form a similar picture of an individual? My humble opinion: we've lost the sharing of the deeply personal things that make us human.

Look at today's platforms: primarily, Twitter, Instagram, and other "microblogging"[^3] services. Such platforms emphasize brevity in text and mass-sharing of content: more analogous to a newsfeed than a personal journal. Lucrative and addictive. In their mass-media-like emphasis of sharing *everything,* these platforms tend to lose the more meaningful content not meant to be plastered across everyone's front pages. Viewing of content becomes less intentional, so the nature of such content becomes less personal and much more spur-of-the-moment. Each user's page is much closer to a public front, displaying (especially in the case of Instagram) a curated facade of some user's public image, rather than the more introspective bits that reveal who someone really *is.*

That's why I want to keep this site around. While I can never quite capture the unique aspects of early-2000s personal Internet pages, I find myself using this page for a similar purpose. Sure, I treat it as much more public than a completely private journal, and even if I wanted private or friends-only posts, I couldn't do that: the source for this page is in Git, after all! Still, at least at the time of writing, I think I'm sitting in that Goldilocks zone between personal introspection and public, interpersonal communication. (Especially if we get a couple points of reply in the comments.)[^4]

Indeed, I do think that the spirit of these pages live on today. There will always be those who run their own websites and share personal things there. Tumblr, while quite feed-like in its design (and in some ways, still an absolute cesspool), provided enough of a long-form blog-like context that personal sharing and introspection was possible. (I'm reminded of my friend Danny, who did a lot of longer stuff on there before moving to Twitter.) And, of course, the humble mom blog: often monetized and beaten to death with the "influencer" trend or MLM products...but some of them still keep that beautiful old spirit alive in the purest way, just talking about life stories.

{% figure caption:"I cheated...this is a crop of a years-old photo, but here's that lil' dragon. The crop duplicated a few pixels on the edge...not sure why. Curse you, imagemagick!"%}!["It's 1 AM, can ya blame me for being lazy? :)"](DragonCrop.JPG){%endfigure%}

***
Before I sign off here, here's a thought: I really wish I had the knowledge and will to start doing entries like this back when I was in high school. I used to have a Tumblr then (gasp), but I never quite used it for the sort of content I use my site for now. (That said, I did get started on my State of Affairs-type posts there...) I had some crazy fun memories during my years there - and some harder ones, of course...regardless, it would've been amazing to have such an archive to go back and look through, to remind myself of my mindset at the time. Who mattered most at the time? What was my mind always thinking about? What seemed like the latest world-ending problem? 

A few time points in particular:
- When I was doing that writing project, Islands of Origin (no link, it got buried in an RPNation rebuild)
- When I was obsessed with my work with a project that went to the ISS
- When a certain someone[^5] was infatuated with me...and things happened
- When I realized the manipulative dynamic between said someone and I[^6]
- When cluelessly awkward me actually had a little crush of my own on a close friend[^7]
- When [Olympia](https://3.bp.blogspot.com/--lhcZWR0WP0/U8HilHuSjnI/AAAAAAAAA0w/ad4jOKVLkHY/s1600/Olympia+1.png), my beloved guinea pig, passed[^8]

...upon many more, probably. Such a moving time-capsule would be marvelous, allowing me to peruse my younger mind...or maybe that's just my nostalgic side talking.[^9] :)

***

Mmm, this was a rambly one. Thoughts? Feel free to comment below.

[^1]: I'm hesitant with spiritual assumption due to the many issues of the "God of the gaps" fallacy, but I'm confident beyond a reasonable doubt that God was/is at work here. That said, I should really write about my spirituality sometime - for my own benefit, as well.
[^2]: I really do need to remember how Ash set up the emoji Liquid template on here...can't be bothered at the moment. Also, congrats to anyone who names the musical reference!
[^3]: Gah, what an outdated-sounding term.
[^4]: This also makes me think hard about what I use certain services for. I think I've settled on this, for now: Discord/XMPP as a platform for real-time conversations, Mastodon as a platform for in-the-moment short-form content (much better than Twitter, with it's overly-public style), and my homepage as a place for more creative or introspective long-form stuff. And pictures. And who knows, maybe more!
[^5]: Who shall remain nameless out of respect.
[^6]: Interestingly, I've intentionally avoided this topic in the past. I think it's about time I stop that: while I do not intend to bad-mouth people or build up discussion around their flaws and failings, I also intend to make this page a place of personal honesty, and I can't do that if I keep dancing around sensitive topics. There's an exteremely minute chance the certain someone might find this page, which would be interesting: there are things she is not entirely aware of. I'm willing to risk that chance of personal tension with an old acquaintance, though, in favor of greater honesty with myself and those I'm truly close with who read my ramblings.
[^7]: Who shall remain nameless because...well, we're friends, and there's a very real chance she might stumble across this :) (If you know who you are...well, this is awkward :) Want to grab lunch sometime?)
[^8]: To assure all the piggy-lovers out there: no, she wasn't alone, she had a wonderfully social life in a small herd of 5 pigs. 
[^9]: Though...am I *really* nostalgic for all of those? Probably not the right adjective. Also, how on *earth* did we get to 9 footnotes?!
