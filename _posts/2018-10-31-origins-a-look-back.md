---
title: "Origins: a look back"
tags: retrospective
category: music
comments: true
---

*This is the last post I made on my old Tumblr blog. It's written well enough, so I thought I'd preserve it on my new homepage.*

***

Just over two years ago, I released Origins, the first (and so far, only) album I have ever released. Earlier tonight, I went back for the first time in a while, sat down, and properly listened to it the entire way through. For the first time, I’ve stopped cringing like I always used to when I’d listen and inevitably hear the endless mistakes and areas I could have improved upon. Instead, I found myself seeing past me’s work in a more admirable light. (Wow, that got deep fast.) Here’s what I mean:

Overall, everything on this album was really exploratory and unafraid. I didn’t know enough about audio engineering to know what I was doing wrong, and so I just threw myself at the task of writing an album without stopping to think. In doing so, I was able to look past most of the trouble of professional-quality perfection, especially in sound design; I simply wasn’t able to see it! This allowed me to concentrate all my abilities as pure creative energy; as a result, beginning producer me was able to create a full 7 track EP in 9 months.

Something about past me was able to take rough inspirations and transform them into full-on musical ideas. I’m still trying to figure out what that was. According to my old notes, my inspirations for each track were:

1. Isle Genesis: make a track at all
2. Prelude (The Denizens): Elton John-style piano, carefree/happy/fun. (I seem to remember Animal Crossing’s simple theme having something to do with it too…)
3. Soul Scream: somebody asked me to write a YouTube channel jingle
4. Isle Genesis reprise: total ripoff of Hazel Cronin’s “Dead Horses” album: her title track has a solo reprise of the main melody halfway through the album, and I thought I’d try that idea. (Great album, by the way.)
5. Deity Duel: wrote an energetic hook, but needed to flesh it out. Heard trance producer JayB do a rock remix of some video game song…immediately thought “a rhythm guitar would sound cool here”
6. Aftermath: listened to a ton of electro jams by Jouni Ollila’s BURG project and wanted to copy the general style.
7. Reunion: “hey, most of this album kinda maps out the story for a roleplay project I’m part of…why not extrapolate a bit and have two characters reunite in a fanfiction ending?” The two lead instruments symbolize two different characters from that old RP, actually.

Listening now, instead of hearing all the shortcomings, I’m beginning to appreciate just how wild it was that I could come up with this many ideas and just lay them down on tape, no questions asked. Sure, they could use some polish. You know what, though? I’m liking them more and more just as they are: it’s like a little time capsule of the raw excitement I had back then.

I should take a second to mention something else. I’m not sure if I’ve publicly talked about this before, but Isle Genesis was written during a time in my life when I was involved with a role-play community writing project called Islands of Origin. As the roleplay’s world was fleshed out, I started to draw parallels between my ideas and the theoretical backstory of the fictional world; in return, I noticed the world’s backstory shaping my ideas. I suppose I started to write a concept album without even realizing it, now that I think about it. Islands of Origin gave Isle Genesis a purpose and direction.

Besides the fact that obsessive perfectionist me can now hear the finer mistakes I make, this is probably the reason I *still* haven’t been able to release the 2nd album I’ve challenged myself to write every year since. I’ve just been sketching ideas with no real topic or overall backbone. This should have been more obvious to me earlier: it’s one of the entire reasons I love the Pet Sounds album by the Beach Boys! Pet Sounds was one of the first albums (to my knowledge) that served as a logical progression of music, rather than a collection of hit singles: a concept album.

Is there a conclusion to this ramble? I’m not sure. Maybe I’ll form a grand plan for my second album. I’ll likely just as soon get rid of it. I’m really not sure what this all means.

I’m certain of one thing, though: for the first time in a long while, I truly enjoyed listening to Origins. Maybe I should make some music to top it.

Time to write that second album.
