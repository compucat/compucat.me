---
title: "State of Affairs May '19: Close but no cigar"
tags: state-of-affairs music wiiu cooking
comments: true
---

Hey again! Um...where did the time go? Finals are over, and I've spent a ton of time with family, that's what! Almost forgot to post this :)

***

So, what's changed since last month?

## Workstation
I have terrible hardware luck, apparently: one of the fans on my old HD 7950 died. I figured it was about time to replace the 7-year-old card anyway, so instead of throwing money at new fans for it, I bought a used Vega 56. This thing is mindblowingly fast - I love how well it works! I'm still hanging onto my semi-working 7950 for now - $12 for a new fan and it'll work in a pinch, or I might be able to sell it. Not sure what I'm going to do there.

Interestingly, this is one of the rare times where I'll replace over repair, even as a "REPAIR ALL THE THINGS" nut. I seem to be running into a lot of those lately, though...

## Laptop
So the ThinkPad T495 is out. I'm....weirdly unfazed by it. Sure, the Zen+ based Ryzen 3500u is cool, but they've just sacrificed too much about the T platform for me to be jealous of it anymore over my A485. Give me my removable batteries and dual RAM sockets and full-size SD card slots dangit!

Also, I've finally gotten around to learning how to use [ZenStates](https://github.com/r4m0n/ZenStates-Linux) - it's a little Python script to poke some Ryzen registers, allowing me to dynamically redefine the target voltage and frequency for each P-state. I've currently set things up to downclock to ~400 MHz at idle, down from the 1.4 GHz (!) idle that's usually defined. [Ash (@quarky)](https://heyquark.com/aboutme) is also testing this on their E485; we'll see if that improves power consumption!

## Music
~~busy busy busy busy busy busy busy busy~~

Mixing other people's music on /r/BedroomBands is always fun! It's also easy to do away from the studio - I feel like I've learned my Sony MDR-7506's well enough to mix pretty much anywhere. (No substitute for monitors...but listening to references and learning your system are more important than having a perfect system, anyway.)

Guitar upgrades are ongoing, too. I swapped out the plastic nut on my MIM Strat with a Tusq XL. (Still mad at myself that I chipped the fretboard slightly...not proud of that.) Also upped the string gauge to a set of 11-54s while I was at it. Over on my Jazz bass, it seems one of my cheapie Chinese volume pots is dying, so I picked up a 3-pack of Bourns push/pull pots online. Current plan is to use the push/pull to swap the pickups into series. Eventually, I'd like to rewire my Strat, too, with a neck mini-rail humbucker, a push/pull, and a modified[^1] version of the switch wiring on Rabea Massaad's signature Chapman ML-3. 

Oh, and I *need* to get on writing short blurbs for all my previous music. (My "Reign Of The Dark" writeup is coming, I swear!)

## TAS
I've always been a fan of [dwangoAC's](https://tas.bot) work with the TASBot community...so I finally decided to stop lurking and actually join in the fun! I'm now working with Ownasaurus and TheMas3212 to debug and improve [TAStm32](https://github.com/Ownasaurus/TAStm32), the new standard in open-source console replay devices. You might see me stream about this, who knows?! Currently debugging why Super Mario World ACE runs don't work right on my devboard...

## Video
As you may have seen from recent fedi toots, I am slowly learning and loving DaVinci Resolve! Got a bit of practice on it by shooting and editing a family member's choir concert. I really wish I could've edited it at home on my workstation, though, instead of abroad on my laptop - my poor Ryzen 2500u really struggled with the huge 50GB 4K XAVC footage from the main cam!

(An aside: I've always been a Nikon guy. I got the chance to borrow a D800 with Nikon's 70-200mm f2.8 VR lens for this shoot...it's an absolutely *phenomenal* video combination! Wish I had a little HDMI field monitor to check my focus better, though. Or focus peaking. Ah well.)

## Cookery
Picked up a cheapie 10" Lodge cast iron skillet, and I am in love! It's no Le Creuset, but I'm not complaining for only ~$15-20, heh. A few coats of seasoning in the oven, and it's been working wonderfully.

## Life
I'm not going to go too deep into the details, but I need somewhere to record this: in short, I've been going through some life issues lately. Some of my old vices have been becoming particularly bothersome, especially in this past spring, and I'm finally working to better understand and defeat them. Next fall, this will involve a project I'm intentionally leaving vague for now. Assuming it works out, though (which I *must* make sure it does), this site might have a new reference section added to it soon.

## Meta (aka webdev)
Okay, I'll admit: I've been stalling for half a year now on open-sourcing my site. Reason being, I need to take a good prune through the Git history to make sure there's no sensitive info in an old commit I'm forgetting about. Also, I need to find a good way to sync post drafts in the Git repo without making them public.[^2]

Assuming I'm successful in cleaning up all the old cruft, you'll probably see the repo go live in a few weeks. (Or not.)

Changelog:
- Move dev work actually over to a dev branch (wow, was that so hard?)
- Add jekyll-feed for automatic Atom feed generation (somebody test this please, I'm not sure if it's working or broken)
- Add jekyll-postfiles to sanely handle assets only used in one post (what a lifesaver)

***
Well, that's all for now. I'm not setting a good track record for myself, but I'll try to be earlier with next month! Leave a comment, if you like.

*Footnotes:*

[^1]: Rabea's switching setup has a great sound to it, but isn't set up for a middle pickup. I'm debating either modifying the selections to be more useful with the middle pickup, using the push/pull as a "middle pickup on/off" switch, or using it kinda like a Fender S1 system for a second switching layer. That'd require swapping out the lever for a SuperSwitch...might be worth it, though!

[^2]: One idea: once I properly set up an OpenVPN tunnel to give my server a public IP, I might selfhost a personal Git server, then split things up such that GitLab hosts the public version of my site, but private dev stuff gets pushed to the personal server instead.