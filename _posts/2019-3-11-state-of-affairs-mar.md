---
title: "State of Affairs Mar '19: Data? What data?"
tags: state-of-affairs music wiiu cooking
comments: true
---

Hey again! Just a quick one this time, life's kinda crazy right now.

***

So, what's changed since last month?

## Workstation
For the longest time, I've had this good ol' 1TB Seagate spinner in my workstation as a storage/scratch drive. I probably should have replaced it years ago (judging by the date code, it's seen...10 years of constant use?), but it's never given me any problems yet!

...until last week, that is. Bad sectors everywhere, corruption slowing reads to a halt, you name it. Fortunately, `ddrescue` saved my bacon and managed to copy off all but about 15 KiB successfully. I also somehow managed to eBay snipe a practically new Samsung 860 QVO for about $50 off retail price. Not gonna lie, having all my scratch storage on SSD is wonderfully, insanely fast. (Super useful for giant sample libraries!)

## Laptop
Update on  the ThinkPad: I am *loving* this thing! Battery life has not proven to be an issue at all. I'm considering changing the hostname, though - it's kind of boring (`alex-laptop`) right now. What can I say? It's a ThinkPad - it just works so well!

...well, with one exception. It seems my luck is quite twisted: my 850 EVO I was using as its system disk *also* decided to fail. In this case, it threw a bad block right in the middle of my Windows system files. Thankfully, it's still well within Samsung's warranty, but I'm back fixing my Windows install and running off of a random hard disk until I get my SSD back. \*sigh\*

(At least it's 7200 rpm.)

## Music
Haven't done anything *too* major lately; the only real big thing I've started working on is a collaboration via Reddit's BedroomBands project. (Specifically, producing a Christian praise/worship track written by /u/cutyflower22, with guitar added by /u/LeftyGuitarist.) Honestly, having my disks fail really stalled any music production for a good while.

I also somehow managed to snap a bass string while recording. Not sure how that one happened - I was playing some pretty heavy gauge strings. Good thing I already finished tracking bass for that worship track!

## Shader project
Progress has stalled on this. Currently waiting to see if a friend can revive their Radeon. Said friend was also exploring the possibility of just writing a SPIRV -> PM4 "assembler" of sorts...who knows what'll happen.

## Wii U hardmodding
I am now in possession of a (supposedly) bricked Wii U - perfect for practice performing the NAND dump hardmod. It's tiny, but I shouldn't have too much of an issue with it. May or may not come in handy for a friend who needs to backup/restore the NAND of a couple kiosk consoles.

## Cookery
Some random bits:

A slightly interesting thing I'm noticing: the (presumably) anodized surface of this heavily used 10" aluminum pan I have seems to be slowly wearing off along with some massive amounts of crud that have accumulated on the surface. No time for pictures right now, but I might edit this post and throw a few in later.

One thing I didn't expect would work well in a stir-fry: butter. It seems slightly odd, but I tried it after seeing it used to make Japanese omurice. It's a surprisingly nice flavor, actually! Adds just enough of that rich fatty depth you usually taste in more European cooking. 

***
Well, that's all for now. Let's hope I'm not quite so late with next month's update, hehehe! Leave a comment if you like.