---
title: "The machines"
category: blag
tags: hardware
comments: true
---
I wouldn't be a self-respecting nerd without a good couple of machines around, now would I? Each has earned their name through our time together, one way or another...

---

***Housekeeping note:** I must've forgotten to brush my servers, since this site is suffering from a fair bit of digital decay. Things to be fixed soonish: migrating from Commento.io to selfhosted Commento, updating some CSS for readability, finally fixing mobile formatting, adding some much-needed Liquid templates, etc etc.*

In the rack:
- `wallpc` is my primary workstation, my Ship of Theseus. It was born from discarded, hand-me-down parts draped across my desk: Windows 7 powered by an Intel Core 2 Quad Q6600 in a Dell/Foxconn motherboard, coupled with 3 GB of DDR2 RAM and a then-special treat: a Corsair Force F120 SSD. Soon after, it was given a proper home with a custom-built wall mount and an XFX Radeon 7750, then christened with its then-fitting name.

  After many iterations, `wallpc` these days is powered by Linux and AMD engineering: a Ryzen 2600 and a Vega 56, to be precise, with a rotating cast of supporting components. It lives in a 4U rackmount case, salvaged from a friend's Ethereum mining project, and has even traveled with me as a live broadcast companion. If not physically, it will always be `wallpc` in spirit.

- `kvm` is my uninventively-named server, christened when it was merely meant to be a hypervisor (with all the real work being done in VMs). It was built some time after `wallpc`'s Gigabyte Z67 motherboard died, necessitating an upgrade to its current Ryzen-based platform. My old i5-2500K was then swapped for my parents' i5-2500 (mostly for its IOMMU): a few eBay'd parts and a 2U chassis later, `kvm` was born. Today, it is my hypervisor (Debian+KVM/Docker), my NAS (RAID1 for now), my media server (Plex might not be FOSS, but it works dang well), and my router (pfSense!), among other things.

- `elsa` is my buildserver...well, build-VM that lives on `kvm`. Originally, she was meant as a scratchpad VM for a friend of mine who needed a Debian environment for experimenting with OpenCV (and was having issues dual-booting her laptop). Said friend never ended up logging in...but many others have in her stead: `elsa`'s sandboxed nature made her the perfect buildserver/scratchpad for connecting with GitLab and sharing with fellow friends in need of CPU time.

- `westen`...well, what can I say? He has his own [landing page](https://westen.compucat.me). `westen` is a DigitalOcean VM that serves as my NGINX reverse proxy and gateway to the broader Internet - some of my housing situations have not provided me with a public IP, necessitating some creative VPN tunnels. Like his namesake, he specializes in solving problems, especially getting anything in or out of anywhere.

Of course, I have a few more mobile machines as well.
- `tutuola` is my daily driver: a ThinkPad A485 with a few upgrades. Chosen for his rugged reliability, power when it counts, and steadfast ability to persevere whatever comes his way, he is the one I rely on for nearly everything I do when not behind my desk. Like `wallpc`, it runs the latest version of Ubuntu, though I often test major changes on `wallpc` first. `tutuola` is my "always ready no matter what" machine: sure, it's running Linux, and I will endlessly tinker with the specific way my software stack is set up, but this is definitely one area where I err towards stability. I absolutely love writing code...or, well, anything with him: that keyboard/trackpad/TrackPoint is a wonderful UX combo. One of my favorite laptops I've ever used.

- `caulfield` is a friend I've only recently met - a 13" MacBook Air from early 2015, simply adorned with a sticker reading "Change maker". I met her in my repair shop: poorly "refurbished" in the past and faced with both water damage and a faulty backlight. She was left behind by her former operator, who chose to venture onward with a younger, brighter Cupertinian companion. I couldn't just leave her there to be buried in e-waste! With much care and a slight bit of surgery to replace a blown backlight fuse, she now stays with me: `tutuola`'s smaller companion. Like her namesake, she is the smaller, less imposing partner[^1]: creative, quietly elegant, with a mind for time. (How I love extended battery life.)

- `engeler` is a machine I've just built...rather, I'm still building it :) I'll write more once it's complete. In a nutshell, it's a 12-core monster of a broadcast video encoder, racked up in a luggable 6U roadcase with an HP TFT7600 RKM and some assorted goodies. Currently troubleshooting GPU issues.

- `eggshell` is a slightly odd duck of a Dell Inspiron 7000 SE. It once bore another (not-overly-memorable) name as my daily driver, my partner in all things academic and musical - in fact, where a Dell logo should sit on its cover, a nearly-perfectly-sized Ableton Live sticker resides. We traveled together for years: from coffee shops and laboratories to ski lodges and aeronautic conferences. (And, of course, it sat proudly atop my live keyboard rig through many, many gigs.) Alas, retirement came early in the form of a faulty 2-in-1 orientation sensor, which intermittently disables the keyboard, along with a faulty display with a penchant for ghost touches. It lives out its days as a testbench for when VMs are not enough, rechristened by Ash (and their breakfast) during a bout with Adelie Linux.

- My iBook G4 came to me when I was working at a different repair shop - like `caulfield`, it was rescued from the recycling bin in hopes of a better life. Unlike `caulfield`, however, this iBook was in mint condition: babied all its life, it sat in the bin with a brand-new battery, complete with OEM charger and documentation. I'll always be a PowerPC fan at heart: how could I not rescue this perfectly preserved point of history? It currently runs Debian sid. Sadly, it has no unique hostname yet: while I do take it out and about every once in a while[^3], nothing has come to mind in our adventures thus far. Perhaps in the near future?

- Lastly, `genesis` is the machine that started it all: a Dell Latitude CPi A-series (A400XT if I recall correctly?) that was given to me when I was a young kid. It was an old, discarded machine at the time, but it served as my introduction to computing. (Or, rather, the first machine that I really could tinker with that wasn't shared.) Surprisingly, up until recently it's remained completely unmodified - I did have to swap the hard disk out after it recently bit the dust for good. (The iBook has an SSD now, and `genesis` has its old 40GB spinner.) I no longer have the Pentium 4 Optiplex that I first learned Linux[^4] on, but I'm pretty sure I'll always keep this little Dell around. For sentimental value, if nothing else.

[^1]: Heh, does this make `tutuola` a punk rocker?
[^3]: Admittedly, cheating by running any heavy compilations or whatnot on my home server via ssh.
[^4]: Technically, I *first* installed Damn Small Linux on an old Pentium III rig, but the first useful machine was that Optiplex. The friend who gave it to me preloaded Ubuntu and Arch on it - sadly I never ended up using Arch, though I plan to give it a proper go at some point. Thanks, Paul!
