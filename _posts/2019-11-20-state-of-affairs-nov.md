---
title: "State of Affairs Nov '19: Broken, broken, bork bork bork"
tags: state-of-affairs music wiiu
comments: true
---

From [Ash's blog](https://heyquark.com/brew/2019/11/20/sdl-audio/): "I’m tossing up commiting to monthy or, uh, fortmonthy updates like my mate Alex did for a while..."\\
D'oh, I've been called out! I kinda forgot about these for far too long. Guess what: I'm back!

***

So, what's changed since last mon...er, 5 months ago? (Geez, it's been long! Apologies if my writing's kinda sloppy...)

## Workstation
Many changes! I've finally gotten to the point where running Linux full-time is feasible for me. A few big things blocking this change:
- Music production! (Ableton Live isn't built for Linux, and my audio interface doesn't have Linux drivers.)
- Video production! (I still have some projects for others I need to finish...)
- Live streaming! (My AVerMedia C027 capture card doesn't have FOSS Linux drivers available.)

This is going to be a bit long-winded...best to summarize in bullet-point form. Previous configuration:
- Ryzen 5 2600 @ 4.0 GHz
- 16 GB DDR4-2400 @ 2666 MHz
- Radeon RX Vega 56
- 120 GB Samsung 850 EVO (Windows boot disk)
- 1 TB Samsung 860 QVO (Windows data disk + Linux partition)
- AVerMedia C027 capture card
- TASCAM US-1800 audio interface

List of changes:
- Fixed fan on Radeon HD 7950 (yes, *that* old 7950) and added alongside Vega 56.
- Linux boot partition moved to 850 EVO.
- 860 QVO converted to single ext4 Linux data partition.
- Windows boot and data partitions converted to qcow2 disk images.
- Windows 10 migrated to a virtual machine (QEMU/KVM on Linux).
- Added Magewell Pro Capture HDMI capture card.
- Connected Native Instruments Audio Kontrol 1 audio interface.

Yes: Windows 10 has finally been isolated into a VM! ~~Right where it belongs.~~ Music production still continues: Ableton will run under Wine (with some bugs), and wine-asio plus Jack is a wonderful low-latency solution. Unfortunately, this does mean that I can no longer use my audio interface - we're still working on RE'ing Linux drivers for the US-1800. TASCAM has been...shall we say, less than willing to share resources. Ah well: if I really want to, I can still fire up Ableton in Windows instead.

In the meantime, for system audio, I've connected the Audio Kontrol 1 - my portable interface, which has drivers in the kernel, woot!

Video production is much simpler: DaVinci Resolve works just fine as long as I have the ROCm OpenCL drivers installed for my Vega 56. There's one major drawback: I don't have a Studio license, so all H.264/5 support is disabled. \*sigh\* I foresee a lot of transcoding in the near future.

Live streaming with OBS still works just fine - in fact, I've been unracking my workstation and bringing it along to local events as a streambox. There's one unfortunate [bug](https://gitlab.freedesktop.org/mesa/mesa/issues/1426) I've been helping to document: essentially, a nasty Mesa memory leak causes kernel memory to balloon in certain use cases, confusing the OOM killer and ultimately locking up the system. My current solution: roll back to Mesa 19.0.8. (Hey, if it works, it works!)

Even though I can't use my C027 in Linux, I'd still like the ability to use it as a second capture card. To that end, I've passed it through to my Windows VM (along with the Radeon HD 7950), which runs a separate instance of OBS. The Windows VM then takes in video from the C027 and converts it to an NDI network video stream, which OBS on the Linux side can then decode and use with only a frame of latency. Huzzah!

Oh, and one final thing: after years and years of using the same old Dell keyboard (I think my family's had it since 2006?), I felt it was time for a proper upgrade. A little bit of shrewd eBay'ing led me to an open-box iKBC CD108BT: full 101-key layout, Cherry MX Clears, simultaneous USB and Bluetooth operation, and a classy black look. Perfect! (The USB socket is even a USB-C instead of a mini or micro - a wonderful little quality touch.) I've had zero issues with it: it's a quality tool in the best sense of the word. It gets out of your way, does its job, and does it reliably. Plus, it feels wonderful under your fingers: all the better!

## Laptop
Battery woes continue: this is the *one* feature from the T495 I'd love in my A485. (That, and the Ryzen 3500u's higher general performance...but it's not *that* much higher.) I've purchased an extended battery to help with this, though. Thanks, hot-swappable battery bay!

Otherwise...yeah, it's been a solid laptop! Nothing much else to say.

## Server
I finally broke down and rented a DigitalOcean droplet, which properly gives me a public IP and allows me to host things. (My ISP doesn't actually give me one; I'm behind a giant NAT. *Thanks, Boingo.*) This is a long-overdue upgrade to let me *finally* fix my hosting problems.

My initial problem, however many moons ago: how do I expose SSH to `elsa` (my buildserver) when I don't have a public IP? My bad solution: spin up a free-trial Google VPS and use a reverse SSH tunnel to expose it publicly. This is a terrible solution for multiple reasons:
- Nested TCP [(why this is bad)](http://sites.inka.de/bigred/devel/tcp-tcp.html)
- Requires each firewalled server to run an SSH client per port forwarded
- Less than perfectly reliable
- Google charges VPS traffic per-GB ($$$)

Obviously, this isn't sustainable. Especially since I added Minecraft traffic on top of it, plus some other things. SSH was laggy, Minecraft would drop connections...it was a mess. What's the better solution, then? Enter `westen`, the DigitalOcean VPS![^1]
- `westen` hosts an NGINX reverse proxy as a frontend. (It's live, navigate over to [westen.compucat.me](http://westen.compucat.me) and see for yourself!)
- `westen` also hosts an OpenVPN server, operating as a site-to-site VPN.
- My local pfSense router connects as a VPN client, exposing my server LAN to `westen` (and vice versa). 
- Any incoming traffic is forwarded from NGINX over the VPN to the desired server.

This is a much better "proper" solution! Not only is it more reliable (OpenVPN is configured for UDP, so no more TCP over TCP silliness), but it's also much more flexible in its operation. Individual servers don't have to worry about how they're visible to the Internet: that's all abstracted through pfSense and NGINX. The servers themselves aren't even publicly exposed to incoming Internet connections - they only receive what NGINX forwards through. In fact, this even allows for a near-seamless migration to local NGINX if/when I have a public IP available for local use! Additionally, the DigitalOcean VPS gives me a good chunk of guaranteed bandwidth that's much, much cheaper than Google's expensive per-GB costs. Finally, since NGINX is running the entire thing, I can host more than one HTTP/S server on the same port by simply configuring NGINX to redirect traffic based on the subdomain. 

Needless to say, I'm pretty chuffed with this change. Some ideas for potential things to host:
- tiny Nextcloud instance
- Mastodon/Pleroma+mastofe
- Plex
- Status indicator for the entire system
- Other misc stuff, maybe? Who knows!

Aside: as part of some recent cleanup, I've finally sent some old towers off for recycling...one of which was my first real buildserver. (An old Core 2 Duo Dell tower...some XPS or another.) That thing served me so well - perfect for when I was learning how to be a sysadmin! (Well, I'm still learning, heh.) Also in the pile was an old Dell Optiplex...I wanna say it was a GX270? In any case, *that* thing was my first desktop - and the first computer I properly learned Linux on! It was a gift from a sysadmin friend when I was much younger, straight out of (at that time) the workplace recyling/reuse bin. Much fun and learning was had with that good old machine. (Not gonna lie, recycling this one was hard - I'm sentimental to a fault, hehe.)

Rest in peace, old friends, and thank you for all you've given me!

## Music
Last we spoke, I claimed that "I feel like I've learned my Sony MDR-7506's well enough to mix pretty much anywhere." Oh, how cute: I've realized I only half-knew their sound before. Sure, I knew it well enough to do *some* mixing on, but I've definitely become more aware of their strengths and shortcomings. You live and learn, I guess.

Outside of the studio, I've been mixing more live sound - mostly at a local church, but I've been making some outside connections as well. The church is a lovely place, but I will forever beg them to add more sound absorption to the drum cage and get rid of those horridly harsh Sabian B8 cymbals.[^2] \*shudder\*

Said church has also given me a great excuse to play live more, both on keyboards and on bass. My Juno is getting a nice workout! The push-pull pot I added to my Jazz bass has been indispensable: I've found myself using the series switch to kill hum in certain EMI-heavy environments and act as a volume/mid boost in others. Love it!

## TAS
TAS things have been slightly stagnant; they've taken a backseat to recent academic work. I did, however, get my replay device working properly - in fact, you've probably seen it on a few livestreams if you've caught them. The main issue: believe it or not, my SNES actually had a few dead pins on the controller port! These pins wouldn't affect regular gameplay, but anything using a multitap or lightgun would fail. Precious few games actually used these pins, but they're crucial for sending extra data during arbitrary-code-exec TAS runs. 

So, what to do? By a (frankly, crazy) series of events, I ended up meeting dwangoAC and friends at SGDQ 2019, even though I missed the deadline to register for tickets. (There was some...shall we say...TASBottery involved in that.) Games were played, food was had, and SNESes were swapped - we ended up trading my SNES for one that had working extra data lines. (The recipient of the trade only needed it for conventional games, which my SNES handled just fine.) Huzzah!

## Video/photography
Not much new here in terms of projects. I have, however, finally stepped up my equipment game. My Nikon D70 was far overdue for an upgrade, and it's been replaced by a used Nikon D7500! Yes, I know: its video autofocus is inferior to Canon/Sony. I'm willing to sacrifice that, though, for amazing still image quality and my existing familarity with Nikon DSLRs. (This thing isn't strictly for video, after all.)

I've absolutely fallen in love with this camera: it's so wonderfully intuitive! I'd forgotten how much fun photography is when you're not constantly fighting the limitations of your old camera. I'll post pictures/video when I have some more worth sharing.

## Meta (aka webdev)
Old cruft has been cleaned up: Git source for this webpage is now live [at this link](https://gitlab.com/compucat/compucat.me)! Sorry about the delay. (I may or may not have had to revert a commit containing highly doxx-able info...)

Unfortunately, Ash's fedi timeline has stopped working. It's broken in the bast due to post-handling bugs, but mainline Mastodon has now deprecated Atom feed support. Bummer. We'll either have to find a way to fix it (using somthing not-Atom) or revert back to a premade widget.

I've recently discovered Jekyll's live preview function - apparently, this was somewhat recently integrated into mainline Jekyll instead of being separated out as a plugin! This vastly simplifies my blog-writing workflow: text editor on one screen (`nano` at the moment) and auto-refreshed web browser on the other. (My previous workflow was restricted to Atom, with its auto-Markdown preview function, combined with manual web browser refreshes for style checks...much much slower.)

Changelog:
- Cleanup Git history
- Make Git repo public
- Fix CI to require Jekyll ~>3.0, as jekyll-paginate-v2 doesn't yet support Jekyll 4.

***
Whew, that was a long one! Now that I'm back, we'll have monthly blog stuff back in the swing of things. (Hopefully.) Leave a comment, if you like.

*Footnotes:*

[^1]: You can tell by my hostnames what TV I've been watching lately :)
[^2]: Mad props to the one drummer who consistently brings in a killer snare and a set of Zildjian Ks. I swear, those things are so easy to mix that it feels like I'm cheating.
