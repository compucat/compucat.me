---
title: "State of Affairs Feb '19: ThinkPads, Keyboards, and Shaders, Oh My!"
tags: state-of-affairs music wiiu
comments: true
---

Hey again! I'm a terribly slow writer, and I need an excuse to get some practice...that's why I'm restarting these monthly update posts, I guess! (This one's a tad late, apologies.)

***

So, what's changed since last month?

## Laptop
Unfortunately, my good little Dell laptop has been experiencing some hardware failures - namely, ghost touches and a faulty 2-in-1 sensor. I've only had it for about 2.75 years...it's a shame. When it works, it's still a great laptop - an Inspiron 13-7353 with an i5-6200u, 8GB DDR3, 500GB SSD...not a bad little machine at all! However, it's hard to rely on a laptop as a daily driver if the 2-in-1 sensor keeps disabling the keyboard when you're trying to take notes. With this and other mechanical issues arising, I determined that it was more cost-effective to retire my Dell than to perform a repair that still left a risk of the problem recurring. 

Enter stage left: the new star of the show, a ThinkPad A485! Yup, I went Ryzen Mobile. In the few weeks I've been using it so far, it's been a delightful little machine. The Ryzen 5 2500U is a solid performer, and transferring my SSD over (along with a 16GB RAM kit) makes for *quite* snappy bootup and UX. My existing Kubuntu install even booted up without a hitch! (There've been reports of certain Raven Ridge laptops having poor Linux support without extensive BIOS modification - apparently I dodged that bullet.) The only real gripe I have with it is the battery life - the Raven Ridge chipset seems to eat a good bit more power than Intel's offerings right now. Besides that, however, AMD makes a much more compelling platform, and I'm happy to trade a couple hours of battery for a better overall product. 

Other random remarks: I *finally* understand the hype about ThinkPad keyboards, they're so dang good! The integrated ThinkShutter is a convenient addition - I'm surprised there's actually driver support to detect the shutter's position in Windows! It's still effective without the driver, though - a welcome sight. I'm finding myself using the TrackPoint more than I thought I would - it tracks much better than older Dell equivalents I've used.

## Music
*Reign Of The Dark* is done! You can check it out over on SoundCloud. The vocals still aren't *quite* perfect...ah well, it needed to be released sooner or later. For a first attempt at Eurobeat, I'm happy with the way it turned out.

One thing I'm *quite* excited about...I've finally purchased one of my dream instruments, a Yamaha S90ES! I've been planning for quite some time to pick up a hardware workstation for multiple purposes: bread-and-butter factory patches, new sound design options, and live performance among them. The S90ES is admittedly old (it's a 2005 model), but its sound engine is *fantastic*, and I've always thoroughly enjoyed playing one whenever I've had the chance to at various venues. My second option, a MOXF8, would have given me a technically more advanced sound engine and greater polyphony, but at the expense of front-panel sliders, the *gorgeous* S700 piano, and the S90ES's far superior key action. (The MOXF felt quite spongy when I demoed it.)

Of course, I can't mention the S90ES without also mentioning JayB's most excellent sound sets. He's truly a master synth programmer, making good use of the dual insertion FX engines available per-voice on the S90ES as creative sound design elements beyond the oscillator/LFO/filter/amplifier paradigm. I've been enjoying deconstructing his patches to learn how hes programmed the sounds he has.

## Shader project
I've started work on a slightly annoying project: better shader compilation for the Wii U, whose GPU is basically a modified Radeon HD 4350. There's really no good way to precompile GLSL shaders ahead of time - Mesa's compiler is so tightly integrated into itself it's impossible to separate it out - and the Wii U's GX2 API exposes no resources for on-the-fly shader compilation. (Why would it? All Wii U apps target the same GPU, after all!) I was really hoping that SPIRV-LLVM would save my butt here (allowing compilation with LLVM'S amdgpu backend)...but it seems that's only designed for OpenCL. Rats.

So, what's to do? Are we doomed to forever use AMD ShaderAnalyzer and copy-paste awfully compiled code into latte-assembler? Maybe not. While playing some Xenoblade Chronicles, I remembered the Dolphin emulator's ability to cache compiled shaders...which would require obtaining shader binaries after having the video driver compile them..."hmm," I thought. Turns out, OpenGL actually has a nice little interface for loading/storing precompiled shader binaries! It's a stupid way of getting the shader, but if you happened to have a Radeon HD 4350 or similar installed in a system, compiling a shader and grabbing the binary would give something that's Wii U compatible, in theory. Running this on Linux would let us use Mesa's shader compiler, too: bound to be much better optimized than ShaderAnalyzer.

Next steps: Learn just enough OpenGL to make a dummy application to compile and grab the shader binary. Obtain a working Radeon HD 4350 (or access to one). Figure out how to translate standard shader binaries into whatever format Nintendo uses (or how to load standard binaries in homebrew). Profit.

***
Well, that's all for now. Let's hope I'm not quite so late with next month's update, hehehe! Leave a comment if you like.