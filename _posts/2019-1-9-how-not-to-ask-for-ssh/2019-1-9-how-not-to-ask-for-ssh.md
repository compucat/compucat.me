---
title: "How Not To Ask for SSH Access; or, a general update on all things CompuCat"
tags: state-of-affairs music
comments: true
---

Hey all! My new homepage is mostly up-and-running right now, so I reckon this makes it the perfect time to resume the "State of Affairs" monthly updates I stopped writing nearly 3 years ago. Here we go!

***

*Fun post title courtesy of my good friend Ep8Script - thanks!*

## Music
It's been over 2 years since the release of my first EP. Every year, I've aimed to produce another EP to follow it.....and every year, I've fallen short. To be quite honest, I've got 24 GB of ideas, but no real vision of what direction to take this project. Nevertheless, I have been working on other music - whether it makes it on the EP is an unknown, but who knows? It might!

### Project #1: *Reign Of The Dark*
I'm not a huge anime guy...but I admit to not-so-recently falling in love with Initial D. I enjoy driving, I enjoy crazy technical talk...and now, it seems I've become a fan of Eurobeat as well. *Reign Of The Dark* is a little fun project to emulate that style: fast-paced fun Japanese dance music with impossibly quick brass leads. What's not to like? The instrumental's actually completely finished - I posted an early demo on [SoundCloud](https://soundcloud.com/compucat/hmm-what-could-this-be) back in June. All that's left is to rerecord the *extremely* pitchy scratch vocals. That's it!

...If only it were that easy. True to form, I've written in a stupid challenge: an extended bridge with a creepy spoken monologue. If you know me, you know that I'm *terrible* at writing lyrics. (And if you don't, well now you know!) This is honestly what's been bottlenecking this track; there's a lot of words to fit in this extended bridge.

Ah well. At least I've got this going for me...

{% figure caption:"Glowing praise from Ash @quarky." %}
![Quote from Ash @quarky: "well reign of the dark is a bop \\ not sure what else you're wanting to know here"](ashrotd.png)
{% endfigure %}

### Project #2: Follow up to *You Gotta Move!*
A while ago, I collaborated with a friend on a silly project called *You Gotta Move!* - essentially, my friend's experience in bootcamp in fictional Broadway form. He's a hobby animator, I'm a music producer...what's not to like? I ended up recording a light musical theater-type jam with Rhodes, light rock guitar, and...vocoder! Yup, all the lead vocals are actually me and my Juno through Ableton's vocoder. Too bad the orchestral opening was cut out of the video version - I didn't do the animation or editing. (There's also a *ton* of loud sound effects over the last chorus. Ah well.)

Out of nowhere, I felt like trying to write a sequel to this...but for a *proper* rock orchestra. And by that, I mean: this is going symphonic-metal. Back when I wrote *Can We Ever Know* with a friend, I mixed together a hard rock/metal drum kit from Ableton's stock drum samples. Combine this with bass/guitar/orchestra/Juno/vocals, and you've got yourself a track! The instrumental's nearly done, apart from a slight ending bit (oh how I love you, chorus repetitions).

[Here's a little preview of one of my favorite sections!](https://soundcloud.com/compucat/preview-vgm-rock-orchestra-theme)

(EDIT 12/21/2021: Link to YouTube video has been removed; the channel it points to is no longer one I am comfortable linking on this blog. Sorry! -admin-alex)


## Computery bits
Since we've last spoken...I've built myself a VM host!

...Okay, it's just a computer I've built with $200 and a lot of scrap pile parts, but hey: it's a VM host, and I'm darn proud of it. After my old Z68 motherboard died and I upgraded my workstation to a Ryzen build, I had a spare 2x4GB of DDR3 and an i5-2500k laying around. One eBay motherboard, a 2U rackmount case, random PSU, and an el-cheapo network card later, and I've got myself a server...right?

Alas, it was not that simple. The eBay motherboard I purchased didn't actually support Intel VT-d (required for PCI passthrough in VMWare ESXi). Bummer. I returned the motherboard and replaced it with a *slightly* different model, only to realize that my loyal i5-2500k has VT-d disabled! Gah. Fortunately, the i5-2500 in my mum's PC works as an excellent substitute. (I wasn't planning on overclocking this server, anyway.)

While I was swapping parts around, I realized...there's another 2x4GB of DDR3 in that PC, and I've also got 4x1GB on the junk pile. After a bit of a switcheroo, mum's PC is now outfitted with the 2500k, 1x4GB DDR3, and 3x1GB DDR3 for a total of 7GB. (The motherboard's non-overclocking, but I was able to raise the Turbo Boost multipliers a bit anyway to get a bit more performance out of it.) This left my server with a handy 13GB of mismatched RAM. Fun! I also threw in an 80GB HDD off the junk pile and 2x2TB disks I pulled from an old Iomega ix2 NAS. (The disks were fine, but security updates have stopped for the little thing...I figured I would put it out of its misery. Rest in peace, little guy!)

This server's been running in my rack for a few months now - it's wonderfully stable! I've currently got a bunch of random VMs running on it, including a pfSense router, NAS, and Minecraft server. I've also got a random scratchpad VM...running Debian Sid, of course. We can't have things *too* stable around here!

## Webdev
It's a bit obvious, but...I've got a homepage now! Huge thanks to Ash for letting me use [their Jekyll-based webpage](https://heyquark.com) as a basis. I'm taking this as an opportunity to learn some basic webdev by customizing/improving the Jekyll/SCSS framework. Some assorted changes so far:

- New fonts (still in progress)
- New CSS background
- Comments powered by Commento.io
- Improved Jekyll header generation
- Header dropdown menus!

On the flipside...mobile support is semi-broken. I *need* to fix that...which requires learning more HTML/CSS...but that's tomorrow's project. ;)

## Food
My living situation's changed in the past year, and I've now got a kitchen! I haven't been able to build a ton of electrical projects lately, and I've found cooking to be quite a nice way to scratch the *make-some-stuff* itch I always get. A few random dishes, sorted/awarded randomly:

- **Old standby**: *Shaoxing/soy/garlic/ginger chicken stir-fry.* I've made this a million times - it's an old favorite of mine, and I love doing variations on it. A bit painful to do on an electric stove, though - my parents' gas stove is *so* much better (high heat, quick temperature changes).
- **Try, try again**: *Lemon-parsley pasta with chicken.* There's a million different ways to make this, and I'm trying to perfect a version of it. I usually end up with a bit too much lemon, though - I *need* to stop using so much lemon juice! I recently acquired a zester, though, which should help add depth to the flavor. Definitely not saving this to cook for two on a special night...no....why would you think that? ;)
- **Easy but cheesy**: *Chicken alfredo.* When you just wanna do something simple, chicken alfredo has got your back. Make pasta, dump in a jar of premade alfredo, add chicken, done, right? Well, that technically *works*, but it's quite bland. I've lately been trying to premarinate the chicken in olive oil/salt/pepper/garlic (when I remember) and enhance the sauce with parmesan/thyme/fresh garlic. My other favorite trick: pan-searing the chicken halfway, slicing it, then letting it finish cooking by simmering in the alfredo. Still easy....but much more flavorful than just "bland cheesy".
- **Pain in the $bodypart**: *Omelettes.* One of my favorite breakfast items, and yet, I always either have one of two issues: omelettes sticking to the pan or omelettes tearing when I fold them. I had a much higher success rate on my parents' old gas stove...it's *so* tempting to blame the electric for my issues. Ah well, better get to practicing - I really want to try making Japanese omurice sometime!

## Writing
Now that I've got this webpage, I intend to try and get back into blog-writing a bit more, if only for the random practice. Will I hold up to this? Who knows. I quite hope so, though. Some say I write pretty well, but I've always wanted to do some casual improvement (esp. to my speedwriting - I obsess over the slightest details sometimes.) Here's to a new start!

***

Well, that's all for now. Guess I'll see you whenever I write next! Leave a comment if you like.



*...Oh, and one more thing: here's a cryptic clue to the reference in the title:*
{% figure caption:"Cryptic clues are the best clues." %}
![Quote from @Whovian93699: "RE SSH: I hate you all. <3"](ssh.png)
{% endfigure %}
