---
title: "Still here..."
category: general
tags: personal meta
comments: true
---

Hi, I'm still alive. The last year was kinda rough - still is, rather - for me. I'll try to write a proper catch-up post at some point, but finals are killing me right now.

---

I still need to do some proper updates to this site and make it look a bit fresher. I did, however, finally fix my broken comments; they're on selfhosted Commento now. After who knows *how* long.

Incidentally, while skimming old posts, it occurred to me that I mentioned an old mentor named Paul last time. I've contacted him again since then - was good to have a chat after so many years. Still need to finish fixing up that shortwave radio he gave me.

See you sooner or later... -Alex
