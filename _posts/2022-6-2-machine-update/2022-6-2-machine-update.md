---
title: "The machines (updated)"
category: blag
tags: hardware
comments: true
---

My machines have changed since we last spoke - here's a brief update.

---

- `wallpc` is still kicking along...but is no longer used as a VFIO workstation. (I forgot to mention this last time, but for years I've run a realtime-capable audio/gaming VM on `wallpc` - most recently, with access to a GTX 1650 Super.) That GPU and VM has now been moved to a different box to free up resources and PCIe bandwidth for Linux.

- `engeler` has been decommissioned from its position as a mobile video encoder - it didn't see enough use, it took up tons of portable rack space, and modern laptops are capable of realtime video encoding all on their own these days. (M1 MacBook with a small rack of Thunderbolt SDI I/O is a fun lil' rig :) It has been moved into the main rack and has taken the place of `kvm` - and also hosts the realtime Windows VM formerly running on `wallpc`.

- `kvm` has been decommissioned for now and is currently missing a boot disk and power supply. Thanks, buddy - you've done well.

- `amos` is a new machine! It's my work-issued laptop, begrudgingly running Windows. This was originally an Asus Q405, but I've since migrated to a ThinkPad T14s Gen 2 AMD, which has been utterly fantastic. (Yay for stupid fast RAM bandwidth - it's on par with Apple M1!) 

  - Speaking of work machines, `kenji`[^1] is a lil' purple HP Stream sitting on my desk. It doesn't do much - just runs MagicMirror.

- `tutuola` feels a bit clunky in comparison to `amos` but is still going strong - though a palmrest replacement would do it good. (First-gen Ryzen's power management hasn't exactly aged well...)

- `caulfield` has given me absolutely zero trouble - slightly surprising, considering her water-damaged past!

- `eggshell` is in rather rough shape. The battery is completely dead, and the frame is suffering from screw inserts that are tearing out of their molded plastic holes - a common issue with Dell's construction. It's currently running Klipper to control my (half-working) franken-delta printer.

- `genesis` and the iBook just keep on going, proud as ever. (Perhaps some friends and I should start up a "retirement club" VPN for these older machines :)

- `pifour` is a Raspberry Pi 4 that floats around doing...things. Hasn't earned a proper hostname yet.

And finally - somehow, last time, I forgot my phone. `stabler` is an iPhone 12 Mini, the everpresent practical partner. (It's somewhat ironic that so many of my machines are named after Law and Order characters, considering I haven't actually watched Law and Order for a long time!)

---

[^1]: Named for the perpetually-sidelined Akina SpeedStar, not the chef - my machines are almost all named after fictional characters, but nonetheless I have an immense amount of respect for J. Kenji. (His work is utterly fantastic...and tasty :)
