---
title: "State of Affairs Apr '19: \"I've got your back, Liv\""
tags: state-of-affairs music tech wiiu cooking
comments: true
---

Hey again! Life doesn't slow down, but neither will my monthly updates. I'm not giving up this time, hehe!

***

So, what's changed since last month?

## Server
I've made some changes to my personal VM server. No major hardware changes, unfortunately. (An SSD would definitely be nice for it.) Mainly, I've consolidated some VMs and updated others. pfSense decided to completely die on me, though that may have been my fault for fiddling around with dev builds. I ended up reverting to stable and having to redo my entire configuration. Grr.

Fortunately, however, that was a good excuse to finally get network segregation properly working! I have two public-facing VMs: a Minecraft server for close friends, and a general-purpose development/buildbot VM. These are now on their own subnet and *finally* have been restricted from SSH'ing into my NAS VM. Bwahaha.

(.....I laugh, but this is basic security......ah well, at least I finally did it.)

## Laptop
After using the ThinkPad a bit longer: grr, of *course* AMD just announced Zen+ based mobile APUs with better power consumption! *Right* after I purchase my ThinkPad. Don't get me wrong, battery life hasn't been a complete issue (and I could definitely purchase a second battery for the rear bay), but I'd definitely appreciate a few more hours. Never a bad thing.

Got my SSD back from RMA...apparently they couldn't find an issue. Oh well. If it fails again, I'll RMA it again. It feels *great* to have it back, though; combined with 16 GiB DDR4 and the Ryzen, everything is absurdly snappy.

Oh, and I did end up changing the hostname. `alex-laptop` was boring....it has now been re-christened `tutuola` as an homage to a favorite television series of mine. Props to anyone who knows what it is! (Hint: the seemingly pointless title of this update is also a clue...)

## Music
I've mostly been busy contributing to projects on /r/BedroomBands. "Our Jesus", the Christian praise/worship song with /u/cutyflower22 and /u/Lefty_Guitarist is complete, and [can be listened to here!](https://soundcloud.com/user-223273096/our-jesus) I also mixed a song for /u/jaaaaaaaaaaaash which turned out quite well, though I don't believe it's been released.

Next up: I'm due to play some piano for /u/Mathukey (bit late on that actually) and potentially produce another song for cutyflower. 

Personally, I *do* have a few things coming down the pipeline. I'd eventually like to do another take on Eurobeat with better vocals. (I've actually got the basic song written for that; the production will take a while though. Especially writing the sabi!) I've also got a couple ideas that are ludicrously, annoyingly, ~~deliciously~~ poppy, ranging from synthpop to softer acoustic things. 

Speaking of Eurobeat, I'm also *quite* overdue to do a writeup on "Reign Of The Dark". I know there's a certain Redditor that's been anxiously waiting for it. It's coming, don't worry! (Sorry it's taken so long.) I also need to finally get some stands for my monitors: I've been considering some K&M desktop C-stands, which would allow me to park my S90ES underneath the monitors and free up some desk space. (Right now, the S90ES sits in *front* of the monitors, and I don't have a ton of space beyond that.) I'd use floor stands, but I don't have the room for those.

## Wii U hardmodding
Surprise! As some might have seen on the fediverse, I'm now in posession of a Wii U kiosk console, fully working! This has been sent to me by a good friend for hardmodding to dump the NAND before poking around. The current plan is to use wire-wrap wire to make the incredibly tiny connections to the eMMC and SLC NAND, then stick a tiny piece of perfboard somewhere in the side of the chassis to act as a jumper between those fragile, thin connections and an IDC ribbon cable. All of the active electronics (a Teensy++ for dumping the SLC NAND, and a dummy SD card for the eMMC) can then be put in a little "mod box": detachable and transferrable across multiple hardmodded consoles. I plan to film the modding process and might release a video or writeup if everything turns out well.

I'll be happy if I can do a writeup anywhere close to the kind of thing Ben Heck's done. (Got to meet him a few years back, actually - he's such a neat guy to talk to!)

## Cookery
The weather's finally good enough to grill! Grilled chicken/veggies/potatoes is a particular favorite of mine...it's just so dang tasty. Not much else to say besides that...I made some banana bread, but that's about it.

Assorted photos:

{% figure caption:"Grill time! Honey mustard chicken plus potatoes with herbs and paprika. Irresistible."%}!["Perfectly grilled goodies."](chicken.jpg){%endfigure%}
{% figure caption:"Homemade pizza: fresh mozza, basil, peppers, onions, chicken sausage. *Super* flavorful. On a baking sheet, since I don't have a pizza round."%}!["Delectable homemade pizza."](pizza1.jpg){%endfigure%}
{% figure caption:"A friend came over and made pizza too. Just a margherita with sausage."%}!["More delectable homemade pizza."](pizza2.jpg){%endfigure%}
{% figure caption:"Caprese sandwich, made with leftover pizza ingredients. Served with Cape Cod chips, of course."%}!["A mouthwatering sandwich."](sandwich.jpg){%endfigure%}

(Didn't bother getting my Nikon out - all photos shot on my iPhone SE. Sorry 'bout the exposure on the last one!)

## Meta (aka webdev)
Updated the CSS to make images render a little more nicely. (And by that, I mean not at full-page-width, haha.) This is also using jekyll-figure to generate proper HTML5 figures. There's a niggling bug with no-caption border spacing, though...gah. Need to fix that.

I also have plans to backdate a bunch of posts similar to [LFT's site](https://linusakesson.net) for previous music releases and potentially offer high-quality downloads directly. (Because SoundCloud MP3 streaming sucks...and I also keep getting poked by Ash to do it, heh.)

***
Well, that's all for now. Let's hope I'm not quite so late with next month's update, hehehe! Leave a comment if you like.
