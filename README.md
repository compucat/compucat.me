# compucat.me source code
---
This repo is the source code for https://compucat.me: my personal webpage. This began as a fork of https://heyquark.com (thanks Ash!), but has since been restyled and improved upon. It is a continual work in progress.

Powered by:
- Jekyll (main HTML and post generation framework)
- jekyll-paginate-v2 (front page pagination)
- jekyll-figure (image encapsulation)
- jekyll-feed (Atom feed generation)
- jekyll-postfiles (sane asset handling for files used only in one post)
- GitLab Pages/CI (automated hosting/regeneration)
- CloudFlare (DNS trickery, caching, DDoS prevention)
- heyquark Atom timeline reader
- https://commento.io (hosted comments)
- [heyquark ads](https://ads.heyquark.com)

Custom changes from heyquark:
- Redone CSS/layout/styling
- Automatic topnav generation (including dropdowns)
- Pure CSS carbon-fiber background
- Migration from jekyll-paginate to jekyll-paginate-v2
- Enable Mastodon verification on any page, not just /about

TODO:
- Merge in CW support in updated heyquark timeline
	- And bug Ash to fork this out into its own repo!
- Possibly implement image gallery
- Check/fix bugs on mobile
- Restyle sidebar, likely with post collections ala https://linusakesson.net
- Implement collapsibles with CSS (maybe make them Markdown accessible?) and use them (eg. on lists in music landing page)
- Variablize things (such as mastodon handle, universal links/names, etc.) into a config file
- Add backdated posts for my music projects
- I *think* I'm using a deprecated `<center>` somewhere...fix that
- Implement https://linusakesson.net smart download sections
- Implement smart music file conversion (host source plus ffmpeg)
- Maybe add a Mastodon bot to auto-post a "New blogpost, if you care" message when I do something new? Or not, I like doing that personally...
- Maybe start using Git LFS?
- Add Liquid template to autolink subreddits/usernames