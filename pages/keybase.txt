---
permalink: /keybase.txt
layout: none
---
==================================================================
https://keybase.io/compucat
--------------------------------------------------------------------

I hereby claim:

  * I am an admin of https://compucat.me
  * I am compucat (https://keybase.io/compucat) on keybase.
  * I have a public key with fingerprint 4639 C5B2 DB10 1710 7BCD  5490 30CE 11B4 DD4A 11F8

To do so, I am signing this object:

{
  "body": {
    "key": {
      "eldest_kid": "010191eca8e388a1876b24bafb3d1eb63ae4ad2d127dcfeb00b2a5937b99268d6d290a",
      "fingerprint": "4639c5b2db1017107bcd549030ce11b4dd4a11f8",
      "host": "keybase.io",
      "key_id": "30ce11b4dd4a11f8",
      "kid": "010191eca8e388a1876b24bafb3d1eb63ae4ad2d127dcfeb00b2a5937b99268d6d290a",
      "uid": "29940f75b75fae5a878f0368ef01ae19",
      "username": "compucat"
    },
    "merkle_root": {
      "ctime": 1545290317,
      "hash_meta": "8309a662135faa77ded9b2a6392077d6addc56adf838d4a865b2e309d38129ee",
      "seqno": 4173407
    },
    "revoke": {
      "sig_ids": [
        "a875d8b5392347bf1c6b82a29a45694172956509af1c589ba81085f903c0ba530f"
      ]
    },
    "service": {
      "hostname": "compucat.me",
      "protocol": "https:"
    },
    "type": "web_service_binding",
    "version": 1
  },
  "ctime": 1545290345,
  "expire_in": 157680000,
  "prev": "63931c8acbc563aa750c7aef56f87ed99bd1afeda419d7ee97ed878831fe77f5",
  "seqno": 11,
  "tag": "signature"
}

which yields the signature:

-----BEGIN PGP MESSAGE-----
Version: Keybase OpenPGP v2.0.80
Comment: https://keybase.io/crypto

yMKuAnicrZNbaB1FGMc3iZc0obRgjBF9KKuJt5Mws7OzM3MUlJIWmnoQn+ql8Ti3
PVlyztnT3T3HXIg0GCqFKvjQUvqgtbEEIxYURCJIQ2wppA3Ygg9eoIJ3qOIFBRWN
3wZFKD66D7vMzP/7f//5zc6ZzR1OV9s7a83y+xMrP7edX2k2nb23bI+mXRWbSbc4
7Y7bjY+tGptm5fHIuEUXYYQFtlpySziXmLNAeb6SoSIGWxUQaX1pPIM9ZnRoFULK
k1QQpoTwAm4C4wkk3YIbRvWKTRpJVM/A1g+I0FR5RoE/w4gpbagvEEHaYqx8Y3yJ
ccihcCxO8woIp2Rqh6IY5mBQ3oj3H/r/OXdzw84Twkcho4rRUFoqOeMhIgG3IcLS
YpELU5vUZc2CWse1RlPLzJ0puDWbjFdtOYnjLIersyiXYOpTaEAwgw3KdKxcs5mE
Sk6QkEHgYQJ9JGPGGgHBgJaHYBRIYzSFd8gJhy3zABhaqDGEY09YCzlSu68eu0Uf
M+IjBgkS24rHbd48jSqALXWLj7uwA2q4omBMfKZCrAPFPekJ6dNAQLEnaEAhDKxQ
LpTkGHEaQmSNlKQEhe7oTN4saUV6wzw/p6v2P1TLAzWSOIt1XIWFsSxrpMUcSzbZ
yJVPWVX+26OsorqBnwQqWjZJo7gOmEB5FTGfFlw70YgSW45yBWUBR/DkfWwLLIEV
wZpLrYAUAYgUaSZtSIOQM8AplMEytEb6WBhmrYBJOE5OcGgZC+m/CDGGnLICngCu
LrNmYt2ZleW91zhtXc5117bnt8fp2rT1nzu1rff69akDgxdu+9joHYunF46c6bm4
+aWp0e6ta8vHzHr11ieH35MFr/n7y789vXSh9NOx4NGB0eTAs2e/fmL1hc65Q8mv
d99/5ZnlN946+Nlg/33qtYXFWt/rDlu/c/BV55fF+W1v9p8d++7SV4e/uLdX7v6o
VFpCP16eO//tcos8dmrT8I3u9rnnurt7j1za1Tl8Ap9rP1m6Mv3h8YGLJ/FqxzfT
p++5YYRveVHtr837E8d7+j6dnT/3ygd/7N/xiCPcyuqf/bO79ow8fGjnu28//8Nd
e7oHjp6Sa6V2p3K0VejrrHy50Lx8omfq8E1tkzM3d/TuHJ2PH9IPLn2y5YEA335H
tXMk/v7z3bND+w7+BeS4eWA=
=wiDC
-----END PGP MESSAGE-----

And finally, I am proving ownership of this host by posting or
appending to this document.

View my publicly-auditable identity here: https://keybase.io/compucat

==================================================================
