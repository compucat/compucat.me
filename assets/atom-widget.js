atomfindnode = function(parent, node) {
    return Array.prototype.filter.call(parent.children, function(e) {if (e.nodeName == node) {return true;}});
}

atomwritepage = function(xmldoc) {
    var elems;

    var docroot = xmldoc.documentElement;

    var author = atomfindnode(docroot, "author")[0];
    if (!author) return;

    var authordispname = atomfindnode(author, "poco:displayName")[0].textContent;
    var authoremail = atomfindnode(author, "email")[0].textContent;

    var entries = atomfindnode(docroot, "entry").map(
        function (e) {
            var postauthor, media, share = atomfindnode(e, "activity:verb")[0].textContent == "http://activitystrea.ms/schema/1.0/share";
            if (share) {
                var actobj = atomfindnode(e, "activity:object")[0];
                postauthor = atomfindnode(actobj, "author")[0];
                media = atomfindnode(actobj, "link").filter(function (e) {
                    return e.attributes["rel"].nodeValue == "enclosure";
                }).length > 0;
            } else {
                postauthor = author;
                media = atomfindnode(e, "link").filter(function (e) {
                    return e.attributes["rel"].nodeValue == "enclosure";
                }).length > 0;
            }
            var avatar = atomfindnode(postauthor, "link").filter(function (e) {
                return e.attributes["rel"].nodeValue == "avatar";
            })[0].attributes["href"].nodeValue;
            var url = atomfindnode(e, "link").filter(function (e) {
                return e.attributes["rel"].nodeValue == "alternate" && e.attributes["type"].nodeValue == "text/html";
            })[0].attributes["href"].nodeValue;

            var email = atomfindnode(postauthor, "email")[0].textContent;
            return ({
                share: share,
                dispname: atomfindnode(postauthor, "poco:displayName")[0].textContent,
                email: email,
                url: url,
                media: media,
                avatar: "<img data-atom-img-src=\"" + avatar + "\" title=\"" + email + "'s avatar\">",
                content: atomfindnode(e, "content")[0].textContent,
            });
        }
    );

    template = document.getElementById("atom-entry-template").cloneNode(true);
    template.id = "";

    for (var entry of entries) {
        postelement = template.cloneNode(true);
        for (var property in entry) {
            if (entry.hasOwnProperty(property)) {
                elems = postelement.querySelectorAll("[data-atom-role='" + property + "']");
                for (var elem of elems) {
                    elem.innerHTML = entry[property];
                }
                elems = postelement.querySelectorAll("[data-atom-href='" + property + "']");
                for (var elem of elems) {
                    elem.href = entry[property];
                }
            }
        }

        elems = postelement.querySelectorAll("[data-atom-share]");
        for (var elem of elems) {
            elem.dataset["atomShare"] = "" + entry.share; //thanks JavaScript
        }

        elems = postelement.querySelectorAll("[data-atom-defaultimg]");
        for (var elem of elems) {
            var img = atomfindnode(elem, "IMG")[0];
            img.src = elem.dataset.atomDefaultimg;
        }

        elems = postelement.querySelectorAll("[data-atom-media]");
        for (var elem of elems) {
            elem.dataset["atomMedia"] = "" + entry.media;
        }

        document.getElementById("atom-timeline").appendChild(postelement);
    }
}

atomrun = function(callback) {
    var atomxhr = new XMLHttpRequest();
    atomxhr.onload = function() {
        atomwritepage(atomxhr.responseXML);
        if (callback) callback();
    }
    url = document.getElementById("atom-timeline").dataset.atomurl;
    atomxhr.open("GET", url);
    atomxhr.responseType = "document";
    atomxhr.send();
}
